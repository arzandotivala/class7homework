/**
 * Validates an individual input on form submit
 * @param {HTMLElement} inputEl 
 * @param {Event} submitEvent 
 */
const validateItem = function(inputEl, submitEvent) {
    const errorEl = inputEl.parentElement.querySelector('.error');
  
    if (!inputEl.validity.valid) {
      const labelEl = inputEl.parentElement.querySelector('label');

      if (inputEl.value.length <3) {

        errorEl.innerHTML = `${labelEl.innerText} is Required`;
      } else {
        errorEl.innerHTML = `${labelEl.innerText} is Not Valid`;
      }
      errorEl.classList.add('d-block');
  
      // Prevent form submit
      submitEvent.preventDefault();
  
    } else {
      errorEl.innerHTML = '';
      errorEl.classList.remove('d-block');
    }
  }
  
  const inputElements = document.getElementsByClassName('validate-input');
  
  const formEl = document.getElementById('connect-form')
      .addEventListener('submit', function(e) {
        for (let i = 0; i < inputElements.length; i++) {
          validateItem(inputElements[i], e);
        }
        e.preventDefault();
      });

      const getMessage = function(count, dealerCard) {
        return `Dealer showing ${dealerCard.displayVal}, your count is ${count}.  Draw card?`
      }