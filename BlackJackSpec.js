// Kept the starter Jasmine Player Spec Code
// Update this!
// Kept the starter Jasmine Player Spec Code
// Update this!

describe("dealerShouldDraw", function() {
  const hand1 = [{ //HAND 1
    suit: 'spade',
    val: 9,
    displayVal: '9'
  },
  {
    suit: 'heart',
    val: 10,
    displayVal: '10'
  }];

 const hand2 =[{ //HAND 2
  suit: 'spade',
  val: 'Ace',
  displayVal: '11'
 },
  {
    suit: 'club',
    val: 6,
    displayVal: '6'
  }];

  const hand3 =[{ //HAND 3
    suit: 'diamond',
    val: 10,
    displayVal: '10'
   },
    {
      suit: 'club',
      val: 7,
      displayVal: '7'
    }];
    const hand4 =[{ //HAND 4
      suit: 'club',
      val: 2,
      displayVal: '2'
     },
     {
      suit: 'spade',
      val: 4,
      displayVal: '4'
     },
     {
      suit: 'spade',
      val: 2,
      displayVal: '2'
     },
     {
      suit: 'spade',
      val: 5,
      displayVal: '5'
     }];
    expect(calcPoints(hand1).total).toBe(19)
    expect(calcPoints(hand2).total).toBe(17)
    expect(calcPoints(hand3).total).toBe(17)
    expect(calcPoints(hand4).total).toBe(13)
    });


    describe("delaerhouldDraw",function(){
      const hand1 = [{ //HAND 1
        suit: 'spade',
        val: 9,
        displayVal: '9'
      },
      {
        suit: 'heart',
        val: 10,
        displayVal: '10'
      }];
  
  it("should decide whether the dealer should draw", function(){
    expect(dealerShouldDraw(hand1)).toBe(false);
    expect(dealerShouldDraw(hand2)).toBe(true);
    expect(dealerShouldDraw(hand3)).toBe(false);
    expect(dealerShouldDraw(hand4)).toBe(true);
  });
});




