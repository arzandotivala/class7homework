/**
 * Validates an individual input on form submit
 * @param {HTMLElement} inputEl 
 * @param {Event} submitEvent 
 */

const regEx = /\w+@\w+\.\w+/ 

const validateName = function(inputEl, submitEvent) {
    const errorEl = inputEl.parentElement.querySelector('.error');

   // not=!
      const labelEl = inputEl.parentElement.querySelector('label'); 
      if (inputEl.value.length <3) { 

        errorEl.innerHTML = `${labelEl.innerText} needs to be three or more chareceters`;

        errorEl.classList.add('d-block');

        submitEvent.preventDefault();
  
    } else {

      errorEl.innerHTML = '';
      errorEl.classList.remove('d-block');
    }
  }

  const validateEmail = function(inputEl, submitEvent) {
    const errorEl = inputEl.parentElement.querySelector('.error');
    
      const labelEl = inputEl.parentElement.querySelector('label'); 
      if (regEx.test(inputEl.value) === false) {

        errorEl.innerHTML = `This is not a valid email`;

        errorEl.classList.add('d-block');

        submitEvent.preventDefault();
  
    } else {

      errorEl.innerHTML = '';
      errorEl.classList.remove('d-block');
    }
  }

  const validateMessage = function(inputEl, submitEvent) {
    const errorEl = inputEl.parentElement.querySelector('.error');

      const labelEl = inputEl.parentElement.querySelector('label'); 
      if (inputEl.value.length <10) { 

        errorEl.innerHTML = `${labelEl.innerText} needs to be ten or more chareceters`;

        errorEl.classList.add('d-block');

        submitEvent.preventDefault();
  
    } else {

      errorEl.innerHTML = '';
      errorEl.classList.remove('d-block');
    }
  }

  const inputElements = document.getElementsByClassName('validate-input');
  const formEl= document.getElementById('connect-form')
      formEl.addEventListener('submit', function(e) {
      const name = document.getElementById('name');
      const email = document.getElementById('email');
      const message = document.getElementById('message');
      //const message = document.getElementById('name');
      validateName(name, e);
      validateEmail(email,e)
      validateMessage(message,e)
      e.preventDefault();
      });

// Part C drop down list
      const formSelect = document.querySelector('select');
      const OpportunityEls = document.getElementsByClassName('job-opportunity');
      const CodeEls = document.getElementsByClassName('talk-code');
      
      const removeClassFromCollection = function(collection, className) {
        for(let i = 0; i < collection.length; i++) {
          collection[i].classList.remove(className)
        }
      }
      
      const addClassToCollection = function(collection, className) {
        for(let i = 0; i < collection.length; i++) {
          collection[i].classList.add(className)
        }
      }
      
      formSelect.addEventListener('change', function(e) {
        if (this.value === 'job-opportunity') {
          removeClassFromCollection(OpportunityEls, 'd-none');
          addClassToCollection(CodeEls, 'd-none');
      
        } else {
          removeClassFromCollection(CodeEls, 'd-none');
          addClassToCollection(OpportunityEls, 'd-none');
        }
      });

    