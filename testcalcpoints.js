//Example 1 Jasmine Check
describe("calcPoints Function, function() {
 
  it("should calculate points correctly", function() {
    const hand1 = [ 
        { 
            suit: 'hearts',
            val: 10,
            displayVal: '10'
        },
        {
        suit: 'spades',
        val: 7,
        displayVal: '7'
        },
    ];
    const hand2 = [
        {
            suit: 'hearts',
            val: 11,
            displayVal: 'Ace'
        },
        {
            suit: 'spades'
            val: 9,
            displayVal: '9'
        },
    ];
    const hand3 = [
        {
            suit: 'hearts',
            val: 10,
            displayVal: '10'
        },
        {
            suit: 'spades',
            val: 6,
            displayVal: '6'
        },
        {
            suit: 'hearts',
            val: 11,
            displayVal: 'Ace'
        },
    ];
      expect(determineScore(hand1.total).toEqual(17);
      expect(determineScore(hand2.total).toEqual(20);
      expect(determineScore(hand3.total).toEqual(17);
  });
});
